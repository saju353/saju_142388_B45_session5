<?php
// Addslashes start here
$str = "Is your name O'Really";
echo addslashes($str);
echo "<br>";

//Explode start here
$str= "Html css mysql php";
$arr= explode(" ",$str);
echo"<pre>";
print_r($arr);
echo"<pre>";
echo"<br>";

//implode start here
$topics=array("html","css","mysql","php");
$tutorial=implode(",",$topics);
echo"$tutorial.<br>";
var_dump($tutorial);
echo"<br>";

//htmlentities start here
$str="<br> is a html tag";
echo"htmlentities:";
echo htmlentities($str);
echo"<br>";

//trim start here
$str="hello world!";
echo $str."<br>";
echo"trim:";
echo trim($str,"hed");
echo"<br>";

//ltrim start here
$str="hello world!";
echo $str."<br>";
echo"ltrim";
echo ltrim($str,"hello");
echo"<br>";

//rtrim start here
$str="hello world";
echo $str."<br>";
echo"rtrim";
echo rtrim($str,"world");
echo"<br>";

//nl2br start here
echo nl2br("One line.\nAnother line.");
echo"<br>";

//str_pad start here
$str="hello world";
echo "str_pad:";
echo str_pad($str,25,"!");
echo"<br>";

//str_repeat start here
echo"str_repeat:";
echo str_repeat("Hi!",10);
echo"<br>";

//str_replace start here
echo"str_replace:";
echo str_replace("world","rumpa","hello world");
echo"<br>";

//str_spilt start here
echo"str_split:";
print_r(str_split("HELLO"));
echo"<br>";

//strlen start here
echo"strlen:";
echo strlen("hello world!");
echo"<br>";

//strolower start here
echo"strtolower:";
echo strtolower("hello world!");
echo"<br>";

//stroupper start here
echo"strtoupper:";
echo strtoupper("hello world!");
echo"<br>";

//substr_compare start here
echo"substr_compare:";
echo substr_compare("hello world","hello world",0);
echo"<br>";
echo substr_compare("HellO World","hello world",0);
echo"<br>";
echo substr_compare("hello world","hello world",0);
echo"<br>";

//substr_count start here
echo "substr_count:";
echo substr_count("hello world.The world is vry nice","world");
echo"<br>";

//substr_replace start here
$str="hello world";
echo"substr_replace:";
echo substr_replace("hello","world",4);
echo"<br>";

//ucfirst start here
$str="hello world";
echo"ucfirst:";
echo ucfirst($str);
echo"<br>";
//ucwords start here
$str="hello world";
echo"ucwords:";
echo ucwords($str);

?>

