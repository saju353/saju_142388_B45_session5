<?php
//boolean example start here
$decision =true;
if ($decision){
    echo "The decision is true!";
}

$decision =false;
if($decision){
    echo "The decision is false!";
}
//boolean example end here


//Integer xmple is start here
$value1=100;//integer
$value2=55.35;//float
//Integer xmple is end here


//string xmple strat here
$myString1='abcde1234# $value1';
$myString2="abcde1234# $value1";
echo $myString1."<br>";
echo $myString2."<br>";

$heredocString=<<<BITM
   heredoc line1 $value1 <br>
   heredoc line2 $value1 <br>
   heredoc line3 $value1 <br>
BITM;
echo $heredocString;

$nowdocString=<<<'BITM'
 nowdoc line1 $value1 <br>
 nowdoc line2 $value1 <br>
 nowdoc line3 $value1 <br>
BITM;
echo $nowdocString;
//string xmple end here

//array xmple start here
$indexArray = array(1,2,3,4,5,6,7);
print_r($indexArray);

$indexArray = array("toyota","BMW",3,5.5,"jaguar");
print_r($indexArray);

$ageArray = array("rahim"=>25, "moynar ma"=>55,"abul"=>15);
echo "<pre>";
print_r($ageArray);
echo "</pre>";
$ageArray['abul']=35;
echo($ageArray['abul']);
//array xmple end here
?>